let listaSeries = [
    ['Inuyasha', 167, 20],
    ['Naruto', 220, 20],
    ['Sword Art', 25, 25],
    ['Game of Thrones', 73, 50],
    ['The Mandalorian', 16, 45],
]

// Recebe os dados do usuario
let nomeSerie = prompt("Digite o nome da série")
let episodiosAssistidos = parseInt(prompt("Quantos episodios você já viu?"))

// flag que controla se o programa encontrou algum resultado
let encontrou = false

// Itera a lista de séries
for(let serie of listaSeries){
    // compara se o nome da série iterada é o mesmo da que o usuario informou
    if(serie[0] === nomeSerie){ // se for igual
        calculaResposta(serie, episodiosAssistidos)
        
        // marca uma flag que encontrou a série
        encontrou = true

        // se encontrou a série, encerra a execução do for
        break
    }
}

// se não encontrou nenhum resultado, imprime erro
if(!encontrou){
    console.warn("Não encontrou nada!")
}

// declara a função
function calculaResposta(serie, episodiosAssistidos){
    // calcula o numero de episodios que faltam pra assistir
    let episodiosFaltantes = serie[1] - episodiosAssistidos

    // calcula quantidade de minutes que faltam
    let minutosFaltantes = episodiosFaltantes * serie[2]

    // calcula horas faltantes
    let horasFaltantes = minutosFaltantes / 60

    // define a mensagem de retorno pro usuario
    let mensagem = "Você precisa de " + horasFaltantes + " horas para terminar de assistir essa série"

    // imprime a mensagem
    console.log(mensagem)
}