let botao = document.querySelector("#botaoInserir")
botao.addEventListener('click', inserir)

function inserir(){
    // Recebe o valor de todos os inputs em um objeto
    let jogo = {
        "imagem": document.querySelector("#imagem").value,
        "titulo": document.querySelector("#titulo").value,
        "descricao": document.querySelector("#descricao").value
    }

    // Cria os elementos HTML
    let div = document.createElement("div")
    let img = document.createElement("img")
    let h1 = document.createElement("h1")
    let p = document.createElement("p")

    // Define as propriedades/atributos dos elementos
    img.src = jogo.imagem
    h1.textContent = jogo.titulo
    p.textContent = jogo.descricao

    // Atribuir o pai (div) dos elementos criados
    div.appendChild(img)
    div.appendChild(h1)
    div.appendChild(p)

    // Insere a div inteira dentro da section
    let section = document.querySelector("section")
    section.appendChild(div)
}