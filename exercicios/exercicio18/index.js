const meses = [
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto"
]

let selectDia = document.querySelector("#dia")
let selectMes = document.querySelector("#mes")
let selectAno = document.querySelector("#ano")

// Criador de dias
for(let i = 5; i < 26; i++){
    insereOption(i, selectDia)
}

// Criador de anos
for(let i = 1990; i < 2000; i++){
    insereOption(i, selectAno)
}

for(let mes of meses){
    insereOption(mes, selectMes)
}

meses.forEach(mes => insereOption(mes, selectMes))

function insereOption(valor, pai){
    let option = document.createElement("option")
    option.textContent = valor
    option.value = valor
    pai.appendChild(option)
}