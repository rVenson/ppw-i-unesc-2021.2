const url = "https://parallelum.com.br/fipe/api/v1/carros/marcas"
const requisicao = fetch(url)
const dataList = document.querySelector("#listaMarcas")

requisicao.then(function(resposta){
  const tratamento = resposta.json()

  tratamento.then(function(dado){
      for(let marca of dado){
          adicionarOption(marca.nome)
      }
  })
})

function adicionarOption(nome){
    let option = document.createElement("option")
    option.textContent = nome
    dataList.appendChild(option)
}