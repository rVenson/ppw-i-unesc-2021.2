/*
let notas = {
    "aluno01": [10, 8, 9.5],
    "aluno02": [6, 7, 5.5],
    "aluno03": [0, 7, 9.5]
}

let notas = {
    "casa": {
        "rua": "rua do limao",
        "numero": 100
    }
}

notas.casa.rua // rua do limao

*/


let notas = [
    ["aluno 01", 10, 8, 9.5],
    ["aluno 02", 6, 7, 5.5],
    ["aluno 03", 0, 7, 9.5]
]

for(let aluno of notas){
    let soma = 0
    for(let i = 1; i < 4; i++){
        soma += aluno[i]
    }

    console.log("Soma de " + aluno[0] + " : " + soma)
    if(soma < 18){
        console.log("este aluno foi reprovado")
    }
    
}