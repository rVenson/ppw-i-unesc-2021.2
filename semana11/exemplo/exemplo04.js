/*
let meuElementoTitulo = document.querySelector("#titulo")
console.log(meuElementoTitulo)
console.log(meuElementoTitulo.textContent)
meuElementoTitulo.textContent = "Eu modifiquei o titulo"
*/

function botaoApertado(){
    let meuInput = document.querySelector("input")
    let meuParagrafo = document.querySelector("p")

    //meuParagrafo.textContent = meuInput.value

    document.body.textContent = meuInput.value
}

let meuBotao = document.querySelector("#enviar")
meuBotao.addEventListener('click', botaoApertado)

//meuBotao.onClick = botaoApertado