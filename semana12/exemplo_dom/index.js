let botao = document.querySelector("#cadastrar")

botao.addEventListener('click', function(){
    // recebe o valor digitado no input
    let produto = document.querySelector("#produto").value
    // cria um novo elemento HTML LI na memoria (não insere na arvore aqui)
    let elemento = document.createElement("li")
    // atribui o texto digitado no input ao elemento recem criado
    elemento.textContent = produto
    // pesquisa uma referencia da lista 01
    let ul = document.querySelector("#lista01")
    // a partir da lista, adiciona o elemento HTML LI criado anteriormente
    ul.appendChild(elemento)
})