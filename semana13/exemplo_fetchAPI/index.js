let URL = 'http://g'
let requisicao = fetch(URL)

console.log("fez a requisicao")

requisicao.then(function(resposta){
    let tratamento = resposta.json()
    tratamento.then(function(dados){
        //console.log(dados)
        let nome = dados.name
        let urlImagem = dados.sprites.front_default

        console.log("Inserindo pokemon na pagina")
        inserePokemon(nome, urlImagem)
        console.log("Pokemon inserido com sucesso")
    })
})

function inserePokemon(nome, urlImagem){
    let paragrafo = document.createElement("p")
    paragrafo.textContent = nome
    document.body.appendChild(paragrafo)

    let imagem = document.createElement("img")
    imagem.src = urlImagem
    document.body.appendChild(imagem)
}

console.log("Chegou no final do script")