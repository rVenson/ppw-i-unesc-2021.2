const url = 'https://api-ppw.herokuapp.com/api/0/users'
let usuarios = [
    
]
const listaUsuarioHTML = document.querySelector("#listaUsuario")
const formUsuarioHTML = document.querySelector("#formUsuario")

formUsuarioHTML.addEventListener('submit', function(evento){
    evento.preventDefault()
    let name = document.querySelector("#usuario").value
    let password = document.querySelector("#senha").value

    let usuario = {
        name, password
    }

    postUsuario(usuario)
})

function postUsuario(usuario){
    let requisicao = fetch(url, {
        method: 'POST',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(usuario)
    })

    requisicao.then(function(resposta){
        if(resposta.status == 200){
            usuarios.push(usuario)
            atualizarLista()
            getUsuarioLista()
        }
    })
}

function getUsuarioLista(){
    let requisicao = fetch(url)
    requisicao.then(function(resposta){
        resposta.json().then(function(vetorUsuarios){
            console.log(vetorUsuarios)
            usuarios = vetorUsuarios
            atualizarLista()
        })
    })
}

function imprimirUsuario(nome, senha){
    let paragrafo = document.createElement("p")
    paragrafo.textContent = nome + " - " + senha
    listaUsuarioHTML.appendChild(paragrafo)
}

function atualizarLista(){
    // deletar todos os elementos
    listaUsuarioHTML.innerHTML = ""

    // imprimir lista
    for(let usuario of usuarios){
        imprimirUsuario(usuario.name, usuario.password)
    }
}

getUsuarioLista()