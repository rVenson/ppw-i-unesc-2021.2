const url = 'https://api-ppw.herokuapp.com/api/0/users'
let usuarios = [
    
]
const listaUsuarioHTML = document.querySelector("#listaUsuario")
const formUsuarioHTML = document.querySelector("#formUsuario")

formUsuarioHTML.addEventListener('submit', function(evento){
    evento.preventDefault()
    let inputName = document.querySelector("#usuario")
    let inputPassword = document.querySelector("#senha")

    let usuario = {
        name: inputName.value,
        password: inputPassword.value
    }

    postUsuario(usuario)

    inputName.value = ""
    inputPassword.value = ""
})

function postUsuario(usuario){
    let requisicao = fetch(url, {
        method: 'POST',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(usuario)
    })

    requisicao.then(function(resposta){
        if(resposta.status == 200){
            //usuarios.push(usuario) // usado para atualizar localmente primeiro
            //atualizarLista()
            getUsuarioLista()
        }
    })
}

function getUsuarioLista(){
    let requisicao = fetch(url)
    requisicao.then(function(resposta){
        resposta.json().then(function(vetorUsuarios){
            console.log(vetorUsuarios)
            usuarios = vetorUsuarios
            atualizarLista()
        })
    })
}

function imprimirUsuario(usuario){
    let div = document.createElement("div")
    let botaoDeletar = document.createElement("input")
    let botaoEditar = document.createElement("input")

    // deletar
    botaoDeletar.type = "button"
    botaoDeletar.value = "Deletar"
    /*
    botaoDeletar.addEventListener('click', function(){
        deleteUsuario(usuario._id)
    })
    */
    botaoDeletar.onclick = function(){
        deleteUsuario(usuario._id)
    }

    // editar
    botaoEditar.type = "button"
    botaoEditar.value = "Editar"
    botaoEditar.addEventListener('click', function(){
       let form =  createUserEditor(usuario)
       div.appendChild(form)
    })

    div.textContent = usuario.name + " - " + usuario.password
    div.appendChild(botaoDeletar)
    div.appendChild(botaoEditar)
    listaUsuarioHTML.appendChild(div)
}

function atualizarLista(){
    // deletar todos os elementos
    listaUsuarioHTML.innerHTML = ""

    // imprimir lista
    for(let usuario of usuarios){
        imprimirUsuario(usuario)
    }
}

getUsuarioLista()