function putUsuario(id, usuario){
    const putUrl = url + '/' + id
    let requisicao = fetch(putUrl, {
        method: 'PUT',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(usuario)
    })

    requisicao.then(function(resposta){
        if(resposta.status == 200){
            getUsuarioLista()
        }
    })
}

// Função que cria e retorna um formulario completo
function createUserEditor(usuario){
    // Cria os elementos do formulario na memoria
    let div = document.createElement('div')
    let inputName = document.createElement('input')
    let inputPassword = document.createElement('input')
    let buttonEdit = document.createElement('input')

    // pendura os elementos dentro da div
    div.appendChild(inputName)
    div.appendChild(inputPassword)
    div.appendChild(buttonEdit)

    // atualizar as propriedades de cada elemento
    inputName.type = "text"
    inputName.value = usuario.name
    inputPassword.type = "text"
    inputPassword.value = usuario.password
    buttonEdit.type = "button"
    buttonEdit.value = "Salvar"
    
    // Acopla uma função para confirmar a edicao
    buttonEdit.addEventListener('click', function(){
        let usuarioEditado = {
            name: inputName.value,
            password: inputPassword.value
        }

        putUsuario(usuario._id, usuarioEditado)
    })

    return div
}